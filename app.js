// Теоретичні питання:
// 1. Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// Асинхроність у JS дозволяє обробляти запити на сервер паралельно під час завантаження сторінки, тому ми як користувачі не бачимо зависань і збоїв, коли працюємо з інтерфейсом. Асинхроність реалізується за допомогою колбеків, промісів і async\await.

// Завдання:
// Написати програму "Я тебе знайду по IP"
// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

let url = "https://api.ipify.org/?format=json";

async function findYou(url) {
  let getUrl = await fetch(url);
  let info = await getUrl.json();
  // console.log(info);
  const ip = info.ip;
  let getUrlIp = await fetch(
    `http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`
  );
  let userInfo = await getUrlIp.json();
  //console.log(userInfo);
  function creatElement() {
    const btn = document.querySelector(".btn");
    const { continent, country, regionName, city, district } = userInfo;
    let page = `
        <div>
            <ul>
                <li>${continent}</li>
                <li>${country}</li>
                <li>${regionName}</li>
                <li>${city}</li>
                <li>${district}</li>
            </ul>   
        </div>`;
    btn.addEventListener("click", () => {
      btn.insertAdjacentHTML("afterend", page);
    });
  }
  creatElement(userInfo);
}
findYou(url);
